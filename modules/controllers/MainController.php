<?php

namespace modules\controllers;

use \Controller;

class MainController extends Controller
{
    protected $login;
    public function __construct()
    {
        $this->login = isset($_SESSION["login"]) ? $_SESSION["login"] : '';
        $this->type = isset($_SESSION["type"]) ? $_SESSION["type"] : '';
        $this->model('ruang');
        $this->ruangdata = $this->ruang->getCustom(
          "*",
          "where parent='rawat' and status = 'actived' and prop != 'del'"
        );
        $this->arr = array();
        for ($i=0; $i < sizeof($this->ruangdata) ; $i++) {
          $this->arr[$i]['namaruang'] = $this->ruangdata[$i]->nama;
          $this->arr[$i]['slug'] = $this->ruangdata[$i]->slug;
          $urji = $this->ruang->getUrji($this->ruangdata[$i]->slug);
          $this->arr[$i]['urjip'] =  isset($urji[0]->value) ? $urji[0]->value : null;
        }
    }

    protected function template($viewName, $data = array())
    {
        $view = $this->view('template');
        $view->bind('viewName', $viewName);
        $view->bind('data', array_merge($data, array('ruang' => $this->arr)));
    }
}
