<?php

use \modules\controllers\MainController;

class HomeController extends MainController {

    public function index() {
      $this->model('ruang');
      $this->ruangdata = $this->ruang->getCustom(
        "nama, slug",
        "where parent='rawat' and status = 'actived' and prop != 'del'"
      );
      $data = array();
      $arr = array();
      for ($i=0; $i < sizeof($this->ruangdata) ; $i++) {
        $arr[$i]['namaruang'] = $this->ruangdata[$i]->nama;
        $arr[$i]['slug'] = $this->ruangdata[$i]->slug;
        $arr[$i]['bed'] = $this->ruang->getBed($this->ruangdata[$i]->slug);
        $urji = $this->ruang->getUrji($this->ruangdata[$i]->slug);
        $arr[$i]['urjip'] =  isset($urji[0]->value) ? $urji[0]->value : "";
        $kelas = $this->ruang->getKelas($this->ruangdata[$i]->slug);
        $arr[$i]['kelas'] =  isset($kelas[0]->value) ? $kelas[0]->value : "";
        $arr[$i]['bedcount'] =  count($arr[$i]['bed']);
      }

      $datahome = array();
      foreach ($arr as $value) {
        if ($value['slug'] == "pav_kelas_1" || $value['slug'] == "kartini_kelasvip" ) {
          array_push($datahome,$value);
        }
      }

      $this->template('home', array('data' => $datahome ));
    }

    public function detail() {
      $slug = isset($_GET["uri"]) ? $_GET["uri"] : "";
      $this->model('ruang');
      $this->ruangdata = $this->ruang->getCustom(
        "nama, slug",
        "where parent='rawat' and status = 'actived' and prop != 'del' and slug = '".$slug."'"
      );
      $data = array();
      $arr = array();
      for ($i=0; $i < sizeof($this->ruangdata) ; $i++) {
        $arr[$i]['namaruang'] = $this->ruangdata[$i]->nama;
        $arr[$i]['slug'] = $this->ruangdata[$i]->slug;
        $arr[$i]['bed'] = $this->ruang->getBed($this->ruangdata[$i]->slug);
        $urji = $this->ruang->getUrji($this->ruangdata[$i]->slug);
        $arr[$i]['urjip'] =  isset($urji[0]->value) ? $urji[0]->value : "";
        $kelas = $this->ruang->getKelas($this->ruangdata[$i]->slug);
        $arr[$i]['kelas'] =  isset($kelas[0]->value) ? $kelas[0]->value : "";
        $arr[$i]['bedcount'] =  count($arr[$i]['bed']);
      }
      $this->template('detail', array('data' => $arr ));
    }
}
?>
