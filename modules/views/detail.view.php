
<div class="">


<div class="page-body">
  <div class="row">
    <?php foreach ($data["data"] as $val): ?>
      <div class="col-lg-12">
        <div class="page-header">
          <div class="card">
            <div class="card-block caption-breadcrumb">
                <div class="breadcrumb-header">
                    <h1 class="display-5"><?php echo $val["namaruang"] ?></h1>
                    <h4><?php echo $val["namaruang"] ?> Terdapat <?php echo $val["bedcount"] ?> Bed </h4>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <?php foreach ($val["bed"] as $bednya): ?>
            <?php if ($bednya->terpakai == 1): ?>
              <div class="col-lg-3">
                <div class="card social-card bg-simple-c-pink">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-user f-34 text-c-pink social-icon"></i>
                            </div>
                            <div class="col">
                                <h2 class="m-b-0">BED : <?php echo $bednya->nama; ?></h2>
                                <h4 class="m-b-0">Terpakai</h4>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            <?php else: ?>
              <div class="col-lg-3">
                <div class="card social-card bg-simple-c-blue">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-home f-34 text-c-blue social-icon"></i>
                            </div>
                            <div class="col">
                                <h2 class="m-b-0">BED : <?php echo $bednya->nama; ?></h2>
                                <h4 class="m-b-0">Tersedia</h4>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<!-- Page-body end -->
</div>
<script type="text/javascript">
    function Redirect()
    {
        window.location="<?php echo PATH; ?>";
    }
    // document.write("You will be redirected to a new page in 5 seconds");
    setTimeout('Redirect()', 120000);
</script>
