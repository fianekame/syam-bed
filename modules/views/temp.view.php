<!-- Deluxe end -->
<div class="carousel-item">
  <div class="page-header">
      <div class="row align-items-end">
          <div class="col-lg-8">
              <div class="page-header-title">
                  <div class="d-inline">
                      <h4>Ruang Rawat Inap Deluxe</h4>
                      <span>Ruang Rawat Inap RSUD Syarifah Ambami Rato Ebu</span>
                  </div>
              </div>
          </div>
          <div class="col-lg-4">
              <div class="page-header-breadcrumb">
                  <ul class="breadcrumb-title">
                      <li class="breadcrumb-item">
                          <a href="index-1.htm"> <i class="feather icon-home"></i> </a>
                      </li>
                      <li class="breadcrumb-item"><a href="#!">Ruang Rawat Inap Deluxe</a> </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <!-- Product detail page start -->
          <div class="card product-detail-page">
              <div class="card-block">
                  <div class="row">
                      <div class="col-lg-5 col-xs-12">
                          <div class="port_details_all_img row">
                              <div class="col-lg-12 m-b-15">
                                  <div id="big_banner">
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-1.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-2.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-3.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-4.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-5.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-6.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-7.jpg" alt="Big_ Details">
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 product-right">
                                  <div id="small_banner">
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-1.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-2.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-3.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-4.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-5.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-6.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-7.jpg" alt="small-details">
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-7 col-xs-12 product-detail" id="product-detail">
                          <div class="row">
                              <div>
                                  <div class="col-lg-12">
                                      <!-- <span class="txt-muted d-inline-block">Product Code: <a href="#!"> PRDT1234 </a> </span> -->
                                      <!-- <span class="f-right">Availablity : <a href="#!"> In Stock </a> </span> -->
                                  </div>
                                  <div class="col-lg-12">
                                      <h4 class="pro-desc">Ruang Rawat Inap Tipe Deluxe</h4>
                                  </div>
                                  <div class="col-lg-12">
                                      <span class="text-primary product-price"><i class="icofont icofont-cur-idr"></i>Rp 380.000/Malam</span>
                                      <br> <br>
                                              <div class="m-b-20">
                                                  <h6 class="sub-title m-b-15">Overview</h6>
                                                  <p>
                                                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                  </p>
                                              </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-12">
                                              <div class="m-b-20">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5 class="card-header-text"><i class="icofont icofont-certificate-alt-2 m-r-10"></i> Fasilitas Ruang Rawat</h5>
                                                    </div>
                                                    <div class="card-block revision-block">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <ul class="media-list revision-blc">
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-ghost f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            Drop the IE <a href="#">specific hacks</a> for temporal inputs
                                                                            <div class="media-annotation">4 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-vector f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            Add full font overrides for popovers and tooltips
                                                                            <div class="media-annotation">36 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-share-alt f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            created a new Design branch
                                                                            <div class="media-annotation">36 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-equalizer f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            merged Master and Dev branches
                                                                            <div class="media-annotation">48 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-graph f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            Have Carousel ignore keyboard events
                                                                            <div class="media-annotation">Dec 12, 05:46</div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-12 col-sm-12 mob-product-btn">
                                      <button type="button" class="btn btn-primary waves-effect waves-light m-r-20">
                                          <i class="icofont icofont-cart-alt f-16"></i><span class="m-l-10">Cek Ketersediaan Kamar</span>
                                      </button>
                                      <button type="button" class="btn btn-outline-primary waves-effect waves-light m-r-20" data-toggle="tooltip" title="Add to wishlist">
                                          <i class="icofont icofont-heart-alt f-16 m-0"></i>
                                      </button>
                                      <button type="button" class="btn btn-outline-primary waves-effect waves-light" data-toggle="tooltip" title="Quick view">
                                          <i class="icofont icofont-eye-alt f-16 m-0"></i>
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Product detail page end -->
</div>

<!-- VVIP end -->
<div class="carousel-item">
  <div class="page-header">
      <div class="row align-items-end">
          <div class="col-lg-8">
              <div class="page-header-title">
                  <div class="d-inline">
                      <h4>Ruang Rawat Inap VVIP</h4>
                      <span>Ruang Rawat Inap RSUD Syarifah Ambami Rato Ebu</span>
                  </div>
              </div>
          </div>
          <div class="col-lg-4">
              <div class="page-header-breadcrumb">
                  <ul class="breadcrumb-title">
                      <li class="breadcrumb-item">
                          <a href="index-1.htm"> <i class="feather icon-home"></i> </a>
                      </li>
                      <li class="breadcrumb-item"><a href="#!">Ruang Rawat Inap Deluxe</a> </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <!-- Product detail page start -->
          <div class="card product-detail-page">
              <div class="card-block">
                  <div class="row">
                      <div class="col-lg-5 col-xs-12">
                          <div class="port_details_all_img row">
                              <div class="col-lg-12 m-b-15">
                                  <div id="big_banner1">
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-1.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-2.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-3.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-4.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-5.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-6.jpg" alt="Big_ Details">
                                      </div>
                                      <div class="port_big_img">
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-l-7.jpg" alt="Big_ Details">
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 product-right">
                                  <div id="small_banner1">
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-1.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-2.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-3.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-4.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-5.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-6.jpg" alt="small-details">
                                      </div>
                                      <div>
                                          <img class="img img-fluid" src="vendors\assets\images\product-detail\pro-d-s-7.jpg" alt="small-details">
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-7 col-xs-12 product-detail" id="product-detail">
                          <div class="row">
                              <div>
                                  <div class="col-lg-12">
                                      <!-- <span class="txt-muted d-inline-block">Product Code: <a href="#!"> PRDT1234 </a> </span> -->
                                      <!-- <span class="f-right">Availablity : <a href="#!"> In Stock </a> </span> -->
                                  </div>
                                  <div class="col-lg-12">
                                      <h4 class="pro-desc">Athena Black & Red Polyester Georgette Maxi Dress</h4>
                                  </div>
                                  <div class="col-lg-12">
                                      <span class="text-primary product-price"><i class="icofont icofont-cur-idr"></i>Rp 380.000/Malam</span>
                                      <br> <br>
                                              <div class="m-b-20">
                                                  <h6 class="sub-title m-b-15">Overview</h6>
                                                  <p>
                                                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                  </p>
                                              </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-12">
                                              <div class="m-b-20">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5 class="card-header-text"><i class="icofont icofont-certificate-alt-2 m-r-10"></i> Fasilitas Ruang Rawat</h5>
                                                    </div>
                                                    <div class="card-block revision-block">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <ul class="media-list revision-blc">
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-ghost f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            Drop the IE <a href="#">specific hacks</a> for temporal inputs
                                                                            <div class="media-annotation">4 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-vector f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            Add full font overrides for popovers and tooltips
                                                                            <div class="media-annotation">36 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-share-alt f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            created a new Design branch
                                                                            <div class="media-annotation">36 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex m-b-15">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-equalizer f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            merged Master and Dev branches
                                                                            <div class="media-annotation">48 minutes ago</div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="media d-flex">
                                                                        <div class="p-l-15 p-r-20 d-inline-block v-middle"><a href="#" class="btn btn-outline-primary btn-lg txt-muted btn-icon"><i class="icon-graph f-18 v-middle"></i></a></div>
                                                                        <div class="d-inline-block">
                                                                            Have Carousel ignore keyboard events
                                                                            <div class="media-annotation">Dec 12, 05:46</div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-12 col-sm-12 mob-product-btn">
                                      <button type="button" class="btn btn-primary waves-effect waves-light m-r-20">
                                          <i class="icofont icofont-cart-alt f-16"></i><span class="m-l-10">Cek Ketersediaan Kamar</span>
                                      </button>
                                      <button type="button" class="btn btn-outline-primary waves-effect waves-light m-r-20" data-toggle="tooltip" title="Add to wishlist">
                                          <i class="icofont icofont-heart-alt f-16 m-0"></i>
                                      </button>
                                      <button type="button" class="btn btn-outline-primary waves-effect waves-light" data-toggle="tooltip" title="Quick view">
                                          <i class="icofont icofont-eye-alt f-16 m-0"></i>
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Product detail page end -->
</div>
