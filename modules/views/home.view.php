<!-- 24 sama -->
<div class="">
<div class="page-body">
  <div class="jumbotron">
    <div class="row">
      <div class="col-lg-11">
        <div class="clock">
          <div class="hours">
            <div class="first">
              <div class="number">0</div>
            </div>
            <div class="second">
              <div class="number">0</div>
            </div>
          </div>
          <div class="tick">:</div>
          <div class="minutes">
            <div class="first">
              <div class="number">0</div>
            </div>
            <div class="second">
              <div class="number">0</div>
            </div>
          </div>
          <div class="tick">:</div>
          <div class="seconds">
            <div class="first">
              <div class="number">0</div>
            </div>
            <div class="second infinite">
              <div class="number">0</div>
            </div>
          </div>
        </div>
        <h1 class="display-3">Syamrabu Bed Information System</h1>
        <p class="lead"><?php echo "Data Pertanggal : " . tanggal_indo(date("Y-m-d")) . " Halaman ini akan memperbarui data setiap 5 Menit Sekali."; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <marquee height="700px" direction="up">
    <?php foreach ($data["data"] as $val): ?>
      <div class="col-lg-12">
        <div class="page-header">
          <div class="card">
            <div class="card-block caption-breadcrumb">
                <div class="breadcrumb-header">
                    <h1 class="display-5"><?php echo $val["namaruang"] ?></h1>
                    <h4><?php echo $val["namaruang"] ?> Terdapat <?php echo $val["bedcount"] ?> Bed </h4>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <?php foreach ($val["bed"] as $bednya): ?>
            <?php if ($bednya->terpakai == 1): ?>
              <div class="col-lg-2">
                <div class="card social-card bg-simple-c-pink">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-user f-34 text-c-pink social-icon"></i>
                            </div>
                            <div class="col">
                                <h2 class="m-b-0">BED : <?php echo $bednya->nama; ?></h2>
                                <h5 class="m-b-0">Terpakai</h5>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            <?php else: ?>
              <div class="col-lg-2">
                <div class="card social-card bg-simple-c-green">
                    <div class="card-block">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-home f-34 text-c-green social-icon"></i>
                            </div>
                            <div class="col">
                                <h2 class="m-b-0">BED : <?php echo $bednya->nama; ?></h2>
                                <h5 class="m-b-0">Tersedia</h5>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endforeach; ?>
    </marquee>
  </div>
</div>
<!-- Page-body end -->
</div>
<script type="text/javascript">
    function Redirect()
    {
        window.location="<?php echo PATH; ?>";
    }
    setTimeout('Redirect()', 120000);
</script>
<script type="text/javascript">
var hoursContainer = document.querySelector('.hours')
var minutesContainer = document.querySelector('.minutes')
var secondsContainer = document.querySelector('.seconds')
var tickElements = Array.from(document.querySelectorAll('.tick'))

var last = new Date(0)
console.log(last);
last.setUTCHours(+7)

var tickState = true

function updateTime () {
var now = new Date
console.log(now);

var lastHours = last.getHours().toString()
var nowHours = now.getHours().toString()
if (lastHours !== nowHours) {
  updateContainer(hoursContainer, nowHours)
}

var lastMinutes = last.getMinutes().toString()
var nowMinutes = now.getMinutes().toString()
if (lastMinutes !== nowMinutes) {
  updateContainer(minutesContainer, nowMinutes)
}

var lastSeconds = last.getSeconds().toString()
var nowSeconds = now.getSeconds().toString()
if (lastSeconds !== nowSeconds) {
  //tick()
  updateContainer(secondsContainer, nowSeconds)
}

last = now
}

function tick () {
tickElements.forEach(t => t.classList.toggle('tick-hidden'))
}

function updateContainer (container, newTime) {
var time = newTime.split('')

if (time.length === 1) {
  time.unshift('0')
}


var first = container.firstElementChild
if (first.lastElementChild.textContent !== time[0]) {
  updateNumber(first, time[0])
}

var last = container.lastElementChild
if (last.lastElementChild.textContent !== time[1]) {
  updateNumber(last, time[1])
}
}

function updateNumber (element, number) {
//element.lastElementChild.textContent = number
var second = element.lastElementChild.cloneNode(true)
second.textContent = number

element.appendChild(second)
element.classList.add('move')

setTimeout(function () {
  element.classList.remove('move')
}, 990)
setTimeout(function () {
  element.removeChild(element.firstElementChild)
}, 990)
}

setInterval(updateTime, 100)
</script>
