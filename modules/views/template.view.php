<html lang="en">
<head>
    <title>Syamrabu | Bed Information System </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="vendors\assets\images\favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="vendors\bower_components\bootstrap\css\bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="vendors\assets\icon\feather\css\feather.css">
    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href="vendors\bower_components\slick-carousel\css\slick.css">
    <link rel="stylesheet" type="text/css" href="vendors\bower_components\slick-carousel\css\slick-theme.css">
    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="vendors\assets\icon\simple-line-icons\css\simple-line-icons.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="vendors\assets\icon\themify-icons\themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="vendors\assets\icon\icofont\css\icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="vendors\assets\icon\feather\css\feather.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="vendors\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="vendors\assets\pages\data-table\css\buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="vendors\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css">
    <!-- radial chart -->
    <link rel="stylesheet" href="vendors\assets\pages\chart\radial\css\radial.css" type="text/css" media="all">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="vendors\assets\css\style.css">
    <link rel="stylesheet" type="text/css" href="vendors\assets\css\custom.css">
    <link rel="stylesheet" type="text/css" href="vendors\assets\css\jquery.mCustomScrollbar.css">
</head>

<body>
    <!-- Pre-loader start -->
    <!-- <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Pre-loader end -->
    <div id="pcoded" vertical-nav-type="offcanvas" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                        <li class="header-notification">
                          <button type="button" class="btn btn-primary waves-effect" onclick="location.href='index.php';">Halaman Awal</button>
                        </li>
                        <li class="header-notification">
                          <button type="button" class="btn btn-warning waves-effect" data-toggle="modal" data-target="#large-Modal">Pilih Ruangan</button>
                        </li>
                    </ul>

                    </div>
                </div>
            </nav>

            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                              <div class="modal fade" id="large-Modal" tabindex="-1" role="dialog">
                                  <div class="modal-dialog modal-lg" role="document">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <h4 class="modal-title">Pilih Ruangan</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                                          </div>
                                          <div class="modal-body">
                                            <div class="dt-responsive table-responsive">
                                              <table id="simpletable" class="table table-striped table-bordered nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Ruangan</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php $i = 1; ?>
                                                      <?php foreach ($data["ruang"] as $val): ?>
                                                        <?php if ($val["urjip"] == "URI"): ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><?php echo $val["namaruang"]; ?></td>
                                                            <td><button type="button" class="btn btn-primary btn-sm waves-effect" onclick="location.href='<?php echo PATH; ?>?page=home&&action=detail&&uri=<?php echo $val["slug"]; ?>';">Lihat Bed</button></td>
                                                        </tr>
                                                        <?php $i = $i +1; ?>
                                                        <?php endif; ?>
                                                      <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                              <button type="button" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                                <div class="page-wrapper">
                                  <?php
                                      $view = new View($viewName);
                                      $view->bind('data', $data);
                                      $view->forceRender();
                                  ?>
                                </div>
                                <!-- <div id="styleSelector"> </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script data-cfasync="false" src="..\..\..\cdn-cgi\scripts\5c5dd728\cloudflare-static\email-decode.min.js"></script><script type="text/javascript" src="vendors\bower_components\jquery\js\jquery.min.js"></script>
    <script type="text/javascript" src="vendors\bower_components\jquery-ui\js\jquery-ui.min.js"></script>
    <script type="text/javascript" src="vendors\bower_components\popper.js\js\popper.min.js"></script>
    <script type="text/javascript" src="vendors\bower_components\bootstrap\js\bootstrap.min.js"></script>
    <script type="text/javascript" src="vendors\assets\pages\widget\excanvas.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="vendors\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="vendors\bower_components\modernizr\js\modernizr.js"></script>
    <script type="text/javascript" src="vendors\assets\js\SmoothScroll.js"></script>
    <script src="vendors\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="vendors\assets\js\jquery.mousewheel.min.js"></script>

    <script type="text/javascript" src="vendors\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="vendors\bower_components\modernizr\js\modernizr.js"></script>
    <script type="text/javascript" src="vendors\bower_components\modernizr\js\css-scrollbars.js"></script>

    <!-- barrating js -->
    <script type="text/javascript" src="vendors\bower_components\jquery-bar-rating\js\jquery.barrating.min.js"></script>
    <script type="text/javascript" src="vendors\assets\pages\rating\rating.js"></script>
    <!-- slick js -->
    <script type="text/javascript" src="vendors\bower_components\slick-carousel\js\slick.min.js"></script>
    <!-- product detail js -->
    <script type="text/javascript" src="vendors\assets\pages\product-detail\product-detail.js"></script>
    <!-- data-table js -->
    <script src="vendors\bower_components\datatables.net\js\jquery.dataTables.min.js"></script>
    <script src="vendors\bower_components\datatables.net-buttons\js\dataTables.buttons.min.js"></script>
    <script src="vendors\assets\pages\data-table\js\jszip.min.js"></script>
    <script src="vendors\assets\pages\data-table\js\pdfmake.min.js"></script>
    <script src="vendors\assets\pages\data-table\js\vfs_fonts.js"></script>
    <script src="vendors\bower_components\datatables.net-buttons\js\buttons.print.min.js"></script>
    <script src="vendors\bower_components\datatables.net-buttons\js\buttons.html5.min.js"></script>
    <script src="vendors\bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js"></script>
    <script src="vendors\bower_components\datatables.net-responsive\js\dataTables.responsive.min.js"></script>
    <script src="vendors\bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js"></script>
    <!-- Custom js -->
    <script src="vendors\assets\pages\data-table\js\data-table-custom.js"></script>
    <script src="vendors\assets\js\pcoded.min.js"></script>
    <script src="vendors\assets\js\vartical-layout.min.js"></script>
    <script type="text/javascript" src="vendors\assets\js\script.js"></script>

</body>

</html>
